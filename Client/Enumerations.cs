﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScottLane.MiniNamer.Client
{
    public enum RenameStatus
    {
        Error,
        MissingMetadata,
        Renamed,
        Simulated,
        Skipped
    }

    public enum ItemWhatever
    {
        
    }

    public enum RenameMode
    {
        Rename,
        Simulate
    }
}
