﻿using System;
using System.IO;
using System.Threading;
using ScottLane.MiniNamer.Client.Helpers;
using ScottLane.MiniNamer.Client.Resources;

namespace ScottLane.MiniNamer.Client
{
    /// <summary>
    /// Renames photos and videos based on their creation date.
    /// </summary>
    public class FileRenamer
    {
        private RenameProgress status;
        private RenameSettings settings;

        /// <summary>
        /// Initialises a new instance of the FileRenamer class with the specified settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public FileRenamer(RenameSettings settings)
        {
            status = new RenameProgress();
            this.settings = settings;
        }

        /// <summary>
        /// Renames media files.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The renaming progress.</param>
        public void Rename(CancellationTokenSource cancel, IProgress<RenameProgress> progress)
        {
            status = new RenameProgress();
            DirectoryInfo directory = new DirectoryInfo(settings.Folder);
            UpdateFileCount(directory);
            Rename(directory, cancel, progress);
        }

        /// <summary>
        /// Updates the media file count based on the contents of the specified directory.
        /// </summary>
        /// <param name="directory">The directory to search.</param>
        private void UpdateFileCount(DirectoryInfo directory)
        {
            if (settings.IncludeSubFolders)
            {
                foreach (DirectoryInfo subdirectory in directory.GetDirectories())
                {
                    UpdateFileCount(subdirectory);
                }
            }

            foreach (string extension in settings.Extensions)
            {
                status.FileCount += directory.GetFiles(string.Format("*{0}", extension)).Length;
            }
        }

        /// <summary>
        /// Renames media files in the specified directory.
        /// </summary>
        /// <param name="directory">The directory to search for media files.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="progress">The renaming progress.</param>
        private void Rename(DirectoryInfo directory, CancellationTokenSource cancel, IProgress<RenameProgress> progress)
        {
            if (settings.IncludeSubFolders)
            {
                foreach (DirectoryInfo subdirectory in directory.GetDirectories())
                {
                    Rename(subdirectory, cancel, progress);
                }
            }

            foreach (string extension in settings.Extensions)
            {
                foreach (FileInfo file in directory.GetFiles(string.Format("*{0}", extension)))
                {
                    if (!cancel.IsCancellationRequested)
                    {
                        try
                        {
                            RenameFile(file);
                            status.ErrorMessage = null;
                        }
                        catch (Exception ex)
                        {
                            status.Status = RenameStatus.Error;
                            status.ErrorMessage = ex.Message;
                        }

                        status.FileIndex++;
                        progress.Report(status);
                    }
                }
            }
        }

        /// <summary>
        /// Renames a file and updates the rename status.
        /// </summary>
        /// <param name="file">The file to rename.</param>
        private void RenameFile(FileInfo file)
        {
            status.FileName = file.FullName;
            MediaFile mediaFile = new MediaFile(file);

            if (mediaFile.CreatedOn != default(DateTime))
            {
                string outputName = mediaFile.CreatedOn.ToString(settings.Format);
                string outputFileName = Path.Combine(file.Directory.FullName, string.Concat(outputName, file.Extension.ToLower()));

                // check whether a file will override a different file
                if (System.IO.File.Exists(outputFileName) && outputFileName.ToLower() != file.FullName.ToLower())
                {
                    outputFileName = GetIncrementedFileName(outputFileName, file.FullName, 1);
                }

                // rename the file if it has changed name
                if (outputFileName != file.FullName)
                {
                    if (settings.Mode == RenameMode.Rename)
                    {
                        status.Status = RenameStatus.Renamed;
                        status.NewFileName = outputFileName;
                        file.MoveTo(outputFileName);
                        
                    }
                    else if (settings.Mode == RenameMode.Simulate)
                    {
                        status.Status = RenameStatus.Simulated;
                        status.NewFileName = outputFileName;
                    }
                }
                else
                {
                    status.Status = RenameStatus.Skipped;
                }
            }
            else
            {
                status.Status = RenameStatus.MissingMetadata;
            }
        }

        /// <summary>
        /// Gets a file name that is incremented to avoid duplication.
        /// </summary>
        /// <param name="desiredFileName">The desired file name.</param>
        /// <param name="currentFileName">The current file name.</param>
        /// <param name="increment">The current increment.</param>
        /// <returns>The incremented file name.</returns>
        private string GetIncrementedFileName(string desiredFileName, string currentFileName, int increment)
        {
            FileInfo file = new FileInfo(desiredFileName);
            string incrementedFileName = Path.Combine(file.Directory.FullName, string.Concat(Path.GetFileNameWithoutExtension(file.FullName), "-", increment, file.Extension));  // todo - test

            if (System.IO.File.Exists(incrementedFileName) && incrementedFileName.ToLower() != currentFileName.ToLower())
            {
                incrementedFileName = GetIncrementedFileName(desiredFileName, currentFileName, increment + 1);
            }

            return incrementedFileName;
        }
    }
}

