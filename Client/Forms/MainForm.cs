﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScottLane.MiniNamer.Client.Helpers;
using ScottLane.MiniNamer.Client.Resources;

namespace ScottLane.MiniNamer.Client.Forms
{
    public partial class MainForm : Form
    {
        private bool isRenameRunning;
        private CancellationTokenSource cancel;

        /// <summary>
        /// Initialises the MainForm class.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialises the form controls on load.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            renameStatusToolStripStatusLabel.Text = ClientStrings.StatusReadyText;
            renamePercentageToolStripStatusLabel.Text = "0%";
            RefreshFormControls();
        }

        /// <summary>
        /// Browses for the media item folder.
        /// </summary>
        private void browseButton_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                folderTextBox.Text = folderBrowserDialog.SelectedPath;
            }
        }

        /// <summary>
        /// Validates and refreshes the form after the folder changes.
        /// </summary>
        private void folderTextBox_TextChanged(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = folderTextBox.Text;
            ValidateForm();
            RefreshFormControls();
        }

        /// <summary>
        /// Validates the folder text box.
        /// </summary>
        /// <returns>True if valid, false otherwise.</returns>
        private bool ValidateFolderTextBox()
        {
            bool result = true;

            formErrorProvider.SetError(folderTextBox, string.Empty);

            if (!Directory.Exists(folderTextBox.Text))
            {
                formErrorProvider.SetIconPadding(folderTextBox, -20);
                formErrorProvider.SetError(folderTextBox, ClientStrings.FolderDoesNotExistMessage);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Validates all form controls.
        /// </summary>
        /// <returns>True if valid, false otherwise.</returns>
        private bool ValidateForm()
        {
            bool result = true;

            if (!ValidateFolderTextBox())
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Stops the renaming.
        /// </summary>
        private void stopButton_Click(object sender, EventArgs e)
        {
            cancel.Cancel();
            renameStatusToolStripStatusLabel.Text = ClientStrings.StatusStoppedText;
            RefreshFormControls();
        }

        /// <summary>
        /// Renames media files in the specified folder.
        /// </summary>
        private void renameButton_Click(object sender, EventArgs e)
        {
            RenameAsync(RenameMode.Rename);
            RefreshFormControls();
        }

        /// <summary>
        /// Simulates the renaming of files in the specified folder.
        /// </summary>
        private void simulateOnlyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RenameAsync(RenameMode.Simulate);
            RefreshFormControls();
        }

        /// <summary>
        /// Asynchronously renames media files in the specified folder.
        /// </summary>
        /// <param name="mode">The rename mode.</param>
        private async void RenameAsync(RenameMode mode)
        {
            try
            {
                if (ValidateForm())
                {
                    RenameSettings settings = new RenameSettings()
                    {
                        Extensions = new string[] { ".jpg", ".mov", ".avi" },
                        Folder = folderTextBox.Text,
                        Format = "yyyy-MM-dd HH.mm.ss",
                        IncludeSubFolders = includeSubFoldersCheckBox.Checked,
                        Mode = mode
                    };

                    Logger.Delete();
                    FileRenamer renamer = new FileRenamer(settings);
                    cancel = new CancellationTokenSource();
                    Progress<RenameProgress> progress = new Progress<RenameProgress>();
                    progress.ProgressChanged += progress_ProgressChanged;
                    isRenameRunning = true;
                    RefreshFormControls();
                    renameStatusToolStripStatusLabel.Text = ClientStrings.StatusRunningText;
                    await Task.Run(() => renamer.Rename(cancel, progress));
                    renameStatusToolStripStatusLabel.Text = ClientStrings.StatusCompletedText;
                    DisplayPercentage(100);
                }
            }
            catch
            {
                renameStatusToolStripStatusLabel.Text = ClientStrings.StatusErrorText;
            }
            finally
            {
                isRenameRunning = false;
                RefreshFormControls();
            }
        }

        /// <summary>
        /// Updates form elements based on the renaming progress.
        /// </summary>
        private void progress_ProgressChanged(object sender, RenameProgress e)
        {
            DisplayPercentage(e.Percentage);
            RefreshFormControls();

            if (e.Status == RenameStatus.Renamed)
            {
                Logger.WriteLine(string.Format(ClientStrings.RenamedMessage, e.FileName, Path.GetFileName(e.NewFileName)));
            }
            else if (e.Status == RenameStatus.Simulated)
            {
                Logger.WriteLine(string.Format(ClientStrings.SimulatedMessage, e.FileName, Path.GetFileName(e.NewFileName)));
            }
            else if (e.Status == RenameStatus.Skipped)
            {
                Logger.WriteLine(string.Format(ClientStrings.SkippedMessage, e.FileName));
            }
            else if (e.Status == RenameStatus.MissingMetadata)
            {
                Logger.WriteLine(string.Format(ClientStrings.MetadataMissingMessage, e.FileName));
            }
        }

        /// <summary>
        /// Displays the specified percentage.
        /// </summary>
        /// <param name="percentage">The percentage.</param>
        private void DisplayPercentage(int percentage)
        {
            renameToolStripProgressBar.Value = percentage;
            renamePercentageToolStripStatusLabel.Text = string.Format("{0}%", percentage);
        }

        /// <summary>
        /// Refreshes form controls.
        /// </summary>
        private void RefreshFormControls()
        {
            folderTextBox.Enabled = !isRenameRunning;
            browseButton.Enabled = !isRenameRunning;
            includeSubFoldersCheckBox.Enabled = !isRenameRunning;
            stopButton.Enabled = isRenameRunning;
            renameButton.Enabled = !isRenameRunning;
        }

        /// <summary>
        /// Displays the log.
        /// </summary>
        private void renameToolStripProgressBar_Click(object sender, EventArgs e)
        {
            if (!isRenameRunning && Logger.LogExists())
            {
                Logger.OpenLog();
            }
        }
    }
}
