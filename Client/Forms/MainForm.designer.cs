﻿namespace ScottLane.MiniNamer.Client.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.folderTextBox = new System.Windows.Forms.TextBox();
            this.includeSubFoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.browseButton = new System.Windows.Forms.Button();
            this.folderLabel = new System.Windows.Forms.Label();
            this.renameButton = new wyDay.Controls.SplitButton();
            this.simulateContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.simulateOnlyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.renameStatusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.renameToolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.renamePercentageToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.stopButton = new System.Windows.Forms.Button();
            this.simulateContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formErrorProvider)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // folderTextBox
            // 
            this.folderTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.folderTextBox.Location = new System.Drawing.Point(83, 12);
            this.folderTextBox.Name = "folderTextBox";
            this.folderTextBox.Size = new System.Drawing.Size(300, 20);
            this.folderTextBox.TabIndex = 1;
            this.folderTextBox.TextChanged += new System.EventHandler(this.folderTextBox_TextChanged);
            // 
            // includeSubFoldersCheckBox
            // 
            this.includeSubFoldersCheckBox.AutoSize = true;
            this.includeSubFoldersCheckBox.Location = new System.Drawing.Point(83, 43);
            this.includeSubFoldersCheckBox.Name = "includeSubFoldersCheckBox";
            this.includeSubFoldersCheckBox.Size = new System.Drawing.Size(115, 17);
            this.includeSubFoldersCheckBox.TabIndex = 2;
            this.includeSubFoldersCheckBox.Text = "Include sub-folders";
            this.includeSubFoldersCheckBox.UseVisualStyleBackColor = true;
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Location = new System.Drawing.Point(380, 10);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(32, 23);
            this.browseButton.TabIndex = 4;
            this.browseButton.Text = "...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // folderLabel
            // 
            this.folderLabel.AutoSize = true;
            this.folderLabel.Location = new System.Drawing.Point(12, 15);
            this.folderLabel.Name = "folderLabel";
            this.folderLabel.Size = new System.Drawing.Size(65, 13);
            this.folderLabel.TabIndex = 12;
            this.folderLabel.Text = "Media folder";
            // 
            // renameButton
            // 
            this.renameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.renameButton.AutoSize = true;
            this.renameButton.ContextMenuStrip = this.simulateContextMenuStrip;
            this.renameButton.Location = new System.Drawing.Point(328, 39);
            this.renameButton.Name = "renameButton";
            this.renameButton.Size = new System.Drawing.Size(84, 23);
            this.renameButton.SplitMenuStrip = this.simulateContextMenuStrip;
            this.renameButton.TabIndex = 13;
            this.renameButton.Text = "Rename";
            this.renameButton.UseVisualStyleBackColor = true;
            this.renameButton.Click += new System.EventHandler(this.renameButton_Click);
            // 
            // simulateContextMenuStrip
            // 
            this.simulateContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simulateOnlyToolStripMenuItem});
            this.simulateContextMenuStrip.Name = "renameContextMenuStrip";
            this.simulateContextMenuStrip.Size = new System.Drawing.Size(149, 26);
            // 
            // simulateOnlyToolStripMenuItem
            // 
            this.simulateOnlyToolStripMenuItem.Name = "simulateOnlyToolStripMenuItem";
            this.simulateOnlyToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.simulateOnlyToolStripMenuItem.Text = "Simulate Only";
            this.simulateOnlyToolStripMenuItem.Click += new System.EventHandler(this.simulateOnlyToolStripMenuItem_Click);
            // 
            // formErrorProvider
            // 
            this.formErrorProvider.ContainerControl = this;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameStatusToolStripStatusLabel,
            this.renameToolStripProgressBar,
            this.renamePercentageToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 74);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(424, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // renameStatusToolStripStatusLabel
            // 
            this.renameStatusToolStripStatusLabel.AutoSize = false;
            this.renameStatusToolStripStatusLabel.Name = "renameStatusToolStripStatusLabel";
            this.renameStatusToolStripStatusLabel.Size = new System.Drawing.Size(66, 17);
            this.renameStatusToolStripStatusLabel.Text = "Completed";
            this.renameStatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // renameToolStripProgressBar
            // 
            this.renameToolStripProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.renameToolStripProgressBar.AutoToolTip = true;
            this.renameToolStripProgressBar.Name = "renameToolStripProgressBar";
            this.renameToolStripProgressBar.Size = new System.Drawing.Size(315, 16);
            this.renameToolStripProgressBar.ToolTipText = "Click to view the log file";
            this.renameToolStripProgressBar.Click += new System.EventHandler(this.renameToolStripProgressBar_Click);
            // 
            // renamePercentageToolStripStatusLabel
            // 
            this.renamePercentageToolStripStatusLabel.AutoSize = false;
            this.renamePercentageToolStripStatusLabel.Name = "renamePercentageToolStripStatusLabel";
            this.renamePercentageToolStripStatusLabel.Size = new System.Drawing.Size(35, 17);
            this.renamePercentageToolStripStatusLabel.Text = "100%";
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Location = new System.Drawing.Point(247, 39);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 37;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 96);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.renameButton);
            this.Controls.Add(this.folderLabel);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.includeSubFoldersCheckBox);
            this.Controls.Add(this.folderTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "MiniNamer";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.simulateContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.formErrorProvider)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox folderTextBox;
        private System.Windows.Forms.CheckBox includeSubFoldersCheckBox;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label folderLabel;
        private wyDay.Controls.SplitButton renameButton;
        private System.Windows.Forms.ErrorProvider formErrorProvider;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel renameStatusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar renameToolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel renamePercentageToolStripStatusLabel;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.ContextMenuStrip simulateContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem simulateOnlyToolStripMenuItem;
    }
}

