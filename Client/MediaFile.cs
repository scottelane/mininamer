﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ScottLane.MiniNamer.Client.Parsers;

namespace ScottLane.MiniNamer.Client
{
    /// <summary>
    /// A media file wrapper that exposes a created on date.
    /// </summary>
    public class MediaFile
    {
        private DateTime createdOn;

        /// <summary>
        /// Gets the CreatedOn date of the file according to metadata.
        /// </summary>
        public DateTime CreatedOn { get { return createdOn; } }

        private FileInfo file;

        /// <summary>
        /// Gets the media file.
        /// </summary>
        public FileInfo File { get { return file; } }

        /// <summary>
        /// Initialises a new instance of the MediaFile class.
        /// </summary>
        /// <param name="file">The media file.</param>
        public MediaFile(FileInfo file)
        {
            this.file = file;
            LoadMetadata();
        }

        /// <summary>
        /// Loads metadata from the media file.
        /// </summary>
        private void LoadMetadata()
        {
            string outputExtension = file.Extension.ToLower();

            if (outputExtension == ".jpg")
            {
                using (ExifReader reader = new ExifReader(file.FullName))
                {
                    reader.GetTagValue<DateTime>(ExifTags.DateTimeDigitized, out createdOn);
                }
            }
            else if (outputExtension == ".mov" || outputExtension == ".avi")
            {
                MediaInfo mediaInfo = new MediaInfo();
                mediaInfo.Open(file.FullName);
                string info = mediaInfo.Inform().Trim();
                string[] streams = info.Split(new string[] { string.Concat(Environment.NewLine, Environment.NewLine) }, StringSplitOptions.None);

                foreach (string stream in streams)
                {
                    // only search the general stream
                    if (stream.StartsWith("General"))
                    {
                        string[] propertyStrings = stream.Split(new string[] { string.Concat(Environment.NewLine) }, StringSplitOptions.None);
                        Dictionary<string, string> properties = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

                        foreach (string propertyString in propertyStrings.Skip(1))
                        {
                            string delimeter = ":";
                            string name = propertyString.Substring(0, propertyString.IndexOf(delimeter)).Trim();
                            string value = propertyString.Substring(propertyString.IndexOf(delimeter) + delimeter.Length).Trim();

                            if (!properties.ContainsKey(name))
                            {
                                properties.Add(name, value);
                            }
                        }

                        // multiple data metadata fields can be used in a media file - get highest priority field
                        if (properties.ContainsKey("Recorded date"))
                        {
                            createdOn = GetDateFromString(properties["Recorded date"]);
                            return;
                        }
                        else if (properties.ContainsKey("Encoded date"))
                        {
                            createdOn = GetDateFromString(properties["Encoded date"]);
                            return;
                        }
                        else if (properties.ContainsKey("Mastered date"))
                        {
                            createdOn = GetDateFromString(properties["Mastered date"]);
                            return;
                        }
                    }
                }

                mediaInfo.Close();
            }
        }

        /// <summary>
        /// Gets a DateTime from a date string.
        /// </summary>
        /// <param name="dateString">The date as a string.</param>
        /// <returns>The DateTime.</returns>
        private DateTime GetDateFromString(string dateString)
        {
            DateTime date;
            string utcPrefix = "UTC ";

            if (dateString.StartsWith(utcPrefix))
            {
                dateString = dateString.Substring(utcPrefix.Length);
            }

            if (!DateTime.TryParse(dateString, out date))
            {
                //SAT FEB 01 11:20:24 2014
                string[] dateParts = dateString.Split(' ');

                if (dateParts.Length == 5)
                {
                    date = DateTime.Parse(string.Format("{0}-{1}-{2} {3}", dateParts[4], dateParts[1], dateParts[2], dateParts[3]));
                }
            }

            return date;
        }
    }
}