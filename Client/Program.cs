﻿using System;
using System.Windows.Forms;
using ScottLane.MiniNamer.Client.Forms;

namespace ScottLane.MiniNamer.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
