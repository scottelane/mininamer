﻿using System;

namespace ScottLane.MiniNamer.Client
{
    /// <summary>
    /// Used for reporting rename progress.
    /// </summary>
    public class RenameProgress
    {
        /// <summary>
        /// The error message reported when an error occured renaming a media file.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The total media file count.
        /// </summary>
        public int FileCount { get; set; }

        /// <summary>
        /// The index of the current media file.
        /// </summary>
        public int FileIndex { get; set; }

        /// <summary>
        /// The file name of the current media file.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// The new file name of the current media file.
        /// </summary>
        public string NewFileName { get; set; }

        /// <summary>
        /// The status of the current media file.
        /// </summary>
        public RenameStatus Status { get; set; }

        /// <summary>
        /// The progress percentage.
        /// </summary>
        public int Percentage
        {
            get
            {
                int percentage = 0;

                if (FileCount > 0)
                {
                    percentage = FileIndex * 100 / FileCount;
                }

                return percentage;
            }
        }

        /// <summary>
        /// Initialises a new instance of the RenameProgress class.
        /// </summary>
        public RenameProgress()
        {
            FileCount = 0;
            FileIndex = 0;
        }
    }
}
