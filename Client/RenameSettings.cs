﻿using System;

namespace ScottLane.MiniNamer.Client
{
    /// <summary>
    /// Settings passed to FileRenamer when renaming media files.
    /// </summary>
    public class RenameSettings
    {
        /// <summary>
        /// The filename extensions to rename.
        /// </summary>
        public string[] Extensions { get; set; }

        /// <summary>
        /// The folder to search for media files.
        /// </summary>
        public string Folder { get; set; }

        /// <summary>
        /// The output file format.
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// If true, searches subfolders for media files.
        /// </summary>
        public bool IncludeSubFolders { get; set; }

        /// <summary>
        /// The renaming mode.
        /// </summary>
        public RenameMode Mode { get; set; }

        /// <summary>
        /// Initialises a new instance of the RenameSettings class.
        /// </summary>
        public RenameSettings()
        {
        }
    }
}
